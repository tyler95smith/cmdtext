#pragma once

#include <string>
#include <vector>

using namespace std;

class TextLib{
    public:
        TextLib();
        string printMessage(string input);
        
    private:
        vector<vector<string>> font; // contains the cmd prompt style characters
        vector<string> mesToCmdText(string); // converts a string into a vector<string> version of the original string using cmd prompt style characters
        string lowerCase(string in); // makes any uppercase letters in the string lowercase and returns
        int getFontCharVal(char character); // takes a character and gets the position in the font vector of that same character
};