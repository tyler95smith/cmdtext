#include "TextLib.h"

TextLib::TextLib(){
    vector<string> temp;
    
    // create A character
    temp.push_back(" ___ ");
    temp.push_back("|   |");
    temp.push_back("|___|");
    temp.push_back("|   |");
    temp.push_back("|   |");
    
    font.push_back(temp); // add A character
    temp.clear(); // clear temp to add next character
    
    // create B character
    temp.push_back(" ___ ");
    temp.push_back("|   |");
    temp.push_back("|__/ ");
    temp.push_back("|  \\ ");
    temp.push_back("|___|");
    
    font.push_back(temp); // add B character
    temp.clear(); // clear temp to add next character
    
    // create C character
    temp.push_back(" ____");
    temp.push_back("|    ");
    temp.push_back("|    ");
    temp.push_back("|    ");
    temp.push_back("|____");
    
    font.push_back(temp); // add C character
    temp.clear(); // clear temp to add next character
    
     // create D character
    temp.push_back(" ___ ");
    temp.push_back("|   \\");
    temp.push_back("|   |");
    temp.push_back("|   |");
    temp.push_back("|___/");
    
    font.push_back(temp); // add D character
    temp.clear(); // clear temp to add next character
    
    // create E character
    temp.push_back(" ____");
    temp.push_back("|    ");
    temp.push_back("|____");
    temp.push_back("|    ");
    temp.push_back("|____");
    
    font.push_back(temp); // add E character
    temp.clear(); // clear temp to add next character
    
    // create F character
    temp.push_back(" ____");
    temp.push_back("|    ");
    temp.push_back("|----");
    temp.push_back("|    ");
    temp.push_back("|    ");
    
    font.push_back(temp); // add F character
    temp.clear(); // clear temp to add next character
    
    // create G character
    temp.push_back(" ____");
    temp.push_back("|    ");
    temp.push_back("|  __");
    temp.push_back("|   |");
    temp.push_back("|___/");
    
    font.push_back(temp); // add G character
    temp.clear(); // clear temp to add next character
    
    // create H character
    temp.push_back("     ");
    temp.push_back("|   |");
    temp.push_back("|---|");
    temp.push_back("|   |");
    temp.push_back("|   |");
    
    font.push_back(temp); // add H character
    temp.clear(); // clear temp to add next character
    
    // create I character
    temp.push_back("__ __");
    temp.push_back("  |  ");
    temp.push_back("  |  ");
    temp.push_back("  |  ");
    temp.push_back("__|__");
    
    font.push_back(temp); // add I character
    temp.clear(); // clear temp to add next character
    
    // create J character
    temp.push_back("____ ");
    temp.push_back("    |");
    temp.push_back("    |");
    temp.push_back("|   |");
    temp.push_back("\\___/");
    
    font.push_back(temp); // add J character
    temp.clear(); // clear temp to add next character
    
    // create K character
    temp.push_back("     ");
    temp.push_back("|  / ");
    temp.push_back("|_/  ");
    temp.push_back("| \\  ");
    temp.push_back("|  \\ ");
    
    font.push_back(temp); // add K character
    temp.clear(); // clear temp to add next character
    
    // create L character
    temp.push_back("     ");
    temp.push_back("|    ");
    temp.push_back("|    ");
    temp.push_back("|    ");
    temp.push_back("|____");
    
    font.push_back(temp); // add L character
    temp.clear(); // clear temp to add next character
    
    // create M character
    temp.push_back("     ");
    temp.push_back("|\\ /|");
    temp.push_back("| V |");
    temp.push_back("|   |");
    temp.push_back("|   |");
    
    font.push_back(temp); // add M character
    temp.clear(); // clear temp to add next character
    
    // create N character
    temp.push_back("     ");
    temp.push_back("|\\  |");
    temp.push_back("| \\ |");
    temp.push_back("|  \\|");
    temp.push_back("|   |");
    
    font.push_back(temp); // add N character
    temp.clear(); // clear temp to add next character
    
    // create O character
    temp.push_back(" ___ ");
    temp.push_back("|   |");
    temp.push_back("|   |");
    temp.push_back("|   |");
    temp.push_back("|___|");
    
    font.push_back(temp); // add O character
    temp.clear(); // clear temp to add next character
    
    // create P character
    temp.push_back(" ___ ");
    temp.push_back("|   |");
    temp.push_back("|___|");
    temp.push_back("|    ");
    temp.push_back("|    ");
    
    font.push_back(temp); // add P character
    temp.clear(); // clear temp to add next character
    
    // create Q character
    temp.push_back(" ___ ");
    temp.push_back("|   |");
    temp.push_back("|   |");
    temp.push_back("|   |");
    temp.push_back("|__\\|");
    
    font.push_back(temp); // add Q character
    temp.clear(); // clear temp to add next character
    
     // create R character
    temp.push_back(" ___ ");
    temp.push_back("|   |");
    temp.push_back("|___|");
    temp.push_back("| \\  ");
    temp.push_back("|  \\ ");
    
    font.push_back(temp); // add R character
    temp.clear(); // clear temp to add next character
    
     // create S character
    temp.push_back(" ___ ");
    temp.push_back("|    ");
    temp.push_back("|___ ");
    temp.push_back("    |");
    temp.push_back(" ___|");
    
    font.push_back(temp); // add S character
    temp.clear(); // clear temp to add next character
    
    // create T character
    temp.push_back("_____");
    temp.push_back("  |  ");
    temp.push_back("  |  ");
    temp.push_back("  |  ");
    temp.push_back("  |  ");
    
    font.push_back(temp); // add T character
    temp.clear(); // clear temp to add next character
    
    // create U character
    temp.push_back("     ");
    temp.push_back("|   |");
    temp.push_back("|   |");
    temp.push_back("|   |");
    temp.push_back("|___|");
    
    font.push_back(temp); // add U character
    temp.clear(); // clear temp to add next character
    
    // create V character
    temp.push_back("     ");
    temp.push_back("|   |");
    temp.push_back("\\   /");
    temp.push_back(" \\ / ");
    temp.push_back("  V  ");
    
    font.push_back(temp); // add V character
    temp.clear(); // clear temp to add next character
    
    // create W character
    temp.push_back("     ");
    temp.push_back("|   |");
    temp.push_back("|   |");
    temp.push_back("| ^ |");
    temp.push_back("|/ \\|");
    
    font.push_back(temp); // add W character
    temp.clear(); // clear temp to add next character
    
    // create X character
    temp.push_back("     ");
    temp.push_back("\\   /");
    temp.push_back(" \\ / ");
    temp.push_back(" / \\ ");
    temp.push_back("/   \\");
    
    font.push_back(temp); // add X character
    temp.clear(); // clear temp to add next character
    
    // create Y character
    temp.push_back("     ");
    temp.push_back("|   |");
    temp.push_back("\\___/");
    temp.push_back("  |  ");
    temp.push_back("  |  ");
    
    font.push_back(temp); // add Y character
    temp.clear(); // clear temp to add next character
    
    // create Z character
    temp.push_back(" ___ ");
    temp.push_back("    / ");
    temp.push_back("   / ");
    temp.push_back("  /  ");
    temp.push_back(" /__ ");
    
    font.push_back(temp); // add Z character
    temp.clear(); // clear temp to add next character
    
     // create space character
    temp.push_back("     ");
    temp.push_back("     ");
    temp.push_back("     ");
    temp.push_back("     ");
    temp.push_back("     ");
    
    font.push_back(temp); // add space character
    temp.clear(); // clear temp to add next character
    
    // create invalid character
    temp.push_back("-----");
    temp.push_back("-----");
    temp.push_back("-----");
    temp.push_back("-----");
    temp.push_back("-----");
    
    font.push_back(temp); // add invalid character
    temp.clear(); // clear temp to add next character
}

string TextLib::lowerCase(string in)
{
    string out = in;
    for (int i = 0; i < (int)out.size();i++)
    {
        out[i] = tolower(out[i]);
    }
    return out;
}

int TextLib::getFontCharVal(char character)
{
    character = tolower(character);
    
    if (character == ' ')
        return 26; // the font space character was the 27 object added to the vecot string
        
    if (character < 97 || character > 122) // if character is not valid return question mark
        return 27;
    
    return (int)(character-97); // magic ascii code math that should probably be rewritten so all characters can be used and not just alphabetical
}

vector<string> TextLib::mesToCmdText(string mes)
{
    vector<string> cMes;
    string tempRow;
    int charVal;
    int mes_len = mes.length();
    
    for (int i = 0; i<5;i++) // all 5 rows that the object has
    {
        for (int j=0;j<mes_len;j++) // same row of each character in the message
        {
            charVal = getFontCharVal(mes[j]); // get the int position in the font array of the string character
            tempRow.append(font[charVal][i]);
        }
        cMes.push_back(tempRow); // push the top,second,thrid, etc rows to the final message vector<string>
        tempRow.clear(); // empty the row for reuse in the next loop
    }
    return cMes;
}

string TextLib::printMessage(string input)
{
    string mes = "";
    vector<string> inputVector = mesToCmdText(input);
    for (int j = 0; j < (int)inputVector.size(); j++)
    {
        mes.append(inputVector[j]);
        mes.append("\n");
    }
    
    return mes;
}
